# coding: UTF-8

import numpy as np


class Sigmoid():
    def __init__(self):
        self.has_param = False
        self.y = None

    def __call__(self, x):
        self.y = 1 / (1+np.exp(-x))
        return self.y

    def calc_grad(self, diff):
        return diff * (1-self.y)*self.y


class ReLU():
    def __init__(self):
        self.has_param = False
        self.x = None

    def __call__(self, x):
        self.x = x
        return np.where(x > 0, x, 0)

    def calc_grad(self, diff):
        return diff * np.where(self.x > 0, 1, 0)


class Softmax():
    def __init__(self):
        self.has_param = False
        self.y = None

    def __call__(self, x):
        c = np.expand_dims(np.max(x, axis=1), axis=1)
        exp = np.exp(x-c)
        self.y = exp / np.expand_dims(np.sum(exp, axis=1), axis=1)
        return self.y

    def calc_grad(self, diff):
        return diff * np.sum(self.y)*(1-self.y)
