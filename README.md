### Usage
まず, mnistデータを28×28から784次元のデータにする. 
```sh
    python gen_flatten_mnist.py
```
学習を実行
```sh
    python train.py
```

train.pyのオプションは以下の通り.  
`--epoch` : エポック数  
`--batch` : バッチサイズ  
`--train` : 学習に用いるデータの割合  
`--noise` : ノイズの割合  
`--logdir` : ログの保存先ディレクトリ名  

実行するとログディレクトリに学習曲線, 各値の遷移のcsvファイル(train, testのlossとaccuracy)が保存される.
