# coding: UTF-8

import numpy as np
import torch
from tqdm import tqdm

import torchvision
import torchvision.transforms as transforms

testset = torchvision.datasets.MNIST(
    root='./data', train=True, download=True, transform=transforms.ToTensor())
testloader = torch.utils.data.DataLoader(testset,
                                         batch_size=1,
                                         shuffle=False,)


xs = []
ys = []
for sample in tqdm(testloader):
    x, y = sample
    x = x.view(-1)
    xs.append(x.numpy())
    ys.append(y.numpy()[0])

images = np.array(xs)
labels = np.array(ys)

np.savez('mnist.npz', images=images, labels=labels)
print('completely saved to mnist.npz')
