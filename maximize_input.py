import pickle
from pathlib import Path

import cv2
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from activations import Softmax
from model import MLP
from utils import DataLogger, Optimizer, SoftmaxCrossEntropyLoss, dataloader

np.random.seed(1)

def main():
    with open('model.pickle', 'rb') as p:
        model = pickle.load(p)

    images = []
    for max_index in range(10):
        image = np.zeros((1, 784))
        y = np.expand_dims(np.eye(10)[max_index], axis=0)

        criteria = SoftmaxCrossEntropyLoss()
        opt = Optimizer(model, criteria, lr=1e-2)
        softmax = Softmax()

        for i in tqdm(range(10000)):
            y_pred = softmax(model.forward(image))
            loss = criteria(y, y_pred)
            grad = opt.grad()
            image -= 1e-2 * grad

        images.append(image.reshape((28, 28)))

    plt.figure(figsize=(5,4))
    for i, image in enumerate(images):
        plt.subplot(2, 5, i+1)
        plt.axis('off')
        plt.title(i)
        plt.matshow(image, cmap='gray', fignum=False)

    plt.subplots_adjust(bottom=0.25, top=0.75)
    plt.savefig('max_image.png')
    plt.close()

if __name__ == '__main__':
    main()
