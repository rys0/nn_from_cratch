import numpy as np

from activations import ReLU, Sigmoid, Softmax
from layers import Dense


class MLP():
    def __init__(self, n_units):
        layers = []
        for i in range(len(n_units)-1):
            layers.append(Dense(n_units[i], n_units[i+1]))
            if i != len(n_units) - 1:
                layers.append(ReLU())

        self.layers = layers

    def forward(self, x):
        for l in self.layers:
            x = l(x)
        return x
