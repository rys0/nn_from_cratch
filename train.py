# coding: UTF-8

import argparse
import pickle
from pathlib import Path

import cv2
import numpy as np
from tqdm import tqdm

from activations import Softmax
from model import MLP
from utils import DataLogger, Optimizer, SoftmaxCrossEntropyLoss, dataloader

np.random.seed(1)


def test():
    # train with small dataset

    img_dir = Path('./img')

    images = [cv2.imread('img/{}.png'.format(i), cv2.IMREAD_GRAYSCALE)
              for i in range(5)]
    X = []
    for image in images:
        x = image.reshape(20*20)
        x = x.astype(float) / 255.0
        X.append(x)
    y_true = np.eye(5)

    n_units = [20*20, 200, 5]

    model = MLP(n_units)
    criteria = SoftmaxCrossEntropyLoss()
    opt = Optimizer(model, criteria, lr=1e-2)
    softmax = Softmax()

    X = np.array(X)
    for epoch in range(100000):
        loss = 0
        y_pred = softmax(model.forward(X))
        loss += criteria(y_true, y_pred)
        opt.grad()
        opt.update()

        if epoch % 1000 == 0:
            print(y_pred)
            print(y_true)
            print('epoch: {}, loss: {}'.format(epoch+1, loss/5))


def options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--epoch', type=int, default=100,
                        help='the number of epochs')
    parser.add_argument('--batch', type=int, default=1024, help='batch size')
    parser.add_argument('--train', type=float, default=0.8,
                        help='specify how many datas to use as train data (rate)')
    parser.add_argument('--noise', type=float, default=0.0, help='noise level')
    parser.add_argument('--logdir', type=str, default='log',
                        help='directory name to store logs')
    args = parser.parse_args()
    return args


def train():
    mnist = np.load('mnist.npz')
    images, labels = mnist['images'], mnist['labels']

    args = options()

    train_rate = args.train
    n_classes = len(np.unique(labels))
    batch_size = args.batch
    n_epochs = args.epoch
    save_period = 100
    n_units = [images.shape[1], 200, n_classes]

    # train/test split
    n_train = int(len(images) * train_rate)
    X_train, X_test = images[:n_train], images[n_train:]
    y_train, y_test = labels[:n_train], labels[n_train:]

    # one-hot encoding
    y_train = np.eye(n_classes)[y_train]
    y_test = np.eye(n_classes)[y_test]

    model = MLP(n_units)
    criteria = SoftmaxCrossEntropyLoss()
    opt = Optimizer(model, criteria, lr=1e-2)
    softmax = Softmax()

    print('n_classes: ', n_classes)
    print('train size: ', n_train)
    print('n_epochs: ', n_epochs)
    print('batch_size: ', batch_size)
    print('noise level: ', args.noise)
    print('loss func: CrossEntropyLoss')

    logger = DataLogger(args.logdir)
    pbar = tqdm(range(n_epochs))

    for epoch in pbar:
        loss = 0
        accuracy = 0
        train_loader = dataloader(
            X_train, y_train, batch_size, shuffle=True, noise=args.noise)

        for X, y in train_loader:
            y_pred = softmax(model.forward(X))
            loss += criteria(y, y_pred)
            opt.grad()
            opt.update()
            accuracy += np.sum(np.argmax(y, axis=1) ==
                               np.argmax(y_pred, axis=1))

        train_accuracy = accuracy / len(y_train)
        train_loss = loss / len(y_train)

        # test
        y_pred = softmax(model.forward(X_test))
        test_loss = criteria(y_test, y_pred) / len(y_test)
        test_accuracy = np.sum(np.argmax(y_test, axis=1)
                               == np.argmax(y_pred, axis=1)) / len(y_test)

        logger.add_data('train_accuracy', train_accuracy, epoch)
        logger.add_data('train_loss', train_loss, epoch)
        logger.add_data('test_accuracy', test_accuracy, epoch)
        logger.add_data('test_loss', test_loss, epoch)

        if epoch % save_period == save_period - 1:
            logger.plot_data()
            logger.save_data()

        pbar.set_description('epoch: {}, train_acc: {:.3f}, train_loss: {:.3f},test_acc: {:.3f}, test_loss: {:.3f}'.format(
            epoch, train_accuracy, train_loss, test_accuracy, test_loss))

        with open('model.pickle', mode='wb') as p:
            pickle.dump(model, p)


if __name__ == '__main__':
    train()
