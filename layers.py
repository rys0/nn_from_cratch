# coding: UTF-8

import numpy as np


class Dense():
    def __init__(self, in_units, out_units, weight_initializer=None):
        if weight_initializer is None:
            # use 'He Initialization' by default
            self.weight = np.random.normal(
                0, 2/in_units, (in_units, out_units))
            self.bias = np.zeros(out_units)
        else:
            self.weight, self.bias = weight_initializer(in_units, out_units)

        self.has_param = True
        self.x = None
        self.dW = None
        self.db = None
        self.v = np.zeros(self.weight.shape)
        self.alpha = 0.9

    def __call__(self, x):
        self.x = x
        return np.dot(x, self.weight) + self.bias

    def calc_grad(self, diff):

        self.db = np.sum(diff, axis=0)
        self.dW = np.matmul(self.x.T, diff)

        diff = np.dot(diff, self.weight.T)

        return diff

    def update(self, lr):
        self.v = self.alpha*self.v - lr*self.dW
        self.weight += self.v
        self.bias -= lr*self.db
