import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path


class Optimizer():
    def __init__(self, model, loss_func, lr=1e-2):
        self.model = model
        self.loss_func = loss_func
        self.lr = lr

    def grad(self):
        diff = self.loss_func.diff()

        for l in self.model.layers[::-1]:
            diff = l.calc_grad(diff)

    def update(self):
        for l in self.model.layers:
            if l.has_param:
                l.update(self.lr)
        #self.lr *= 0.999


class SoftmaxCrossEntropyLoss():
    def __call__(self, y_true, y_pred):
        self.y_true = y_true
        self.y_pred = y_pred
        loss = -np.sum(np.log(y_pred) * y_true)
        return loss

    def diff(self):
        return (self.y_pred - self.y_true) / len(self.y_true)


def dataloader(X, y, batch_size, random=True, noise=0.0):
    n_batches = len(X) // batch_size
    if random:
        p = np.random.permutation(len(X))
    else:
        p = range(len(X))

    for b in range(n_batches):
        if b == n_batches - 1:
            X_b = X[p[batch_size*b:]]
            y_b = y[p[batch_size*b:]]
        else:
            X_b = X[p[batch_size*b:batch_size*(b+1)]]
            y_b = y[p[batch_size*b:batch_size*(b+1)]]

        mask = np.random.random_sample(X_b.shape) < noise
        X_b = X_b.copy()
        X_b[mask] = np.random.random_sample(X_b.shape)[mask]

        yield X_b, y_b


class DataLogger():
    def __init__(self, save_dir):
        self.save_dir = Path(save_dir)
        if not self.save_dir.is_dir():
            self.save_dir.mkdir()

        self.iter = {}
        self.data = {}

    def add_data(self, name, data, i=None):
        if i is None:
            i = self.iter.get(name, 0)

        if name not in self.data:
            self.data[name] = np.array([[i, data]])
        else:
            self.data[name] = np.vstack((self.data[name], np.array([i, data])))
        self.iter[name] = i+1

    def save_data(self, fnamehead='scalar_'):
        for key, val in self.data.items():
            fname = fnamehead + key + '.csv'
            np.savetxt(self.save_dir/fname, val)

    def plot_data(self, fnamehead='plot_'):
        for key, val in self.data.items():
            plt.plot(val[:, 0], val[:, 1])
            plt.xlabel('iteration')
            plt.ylabel(key)
            #plt.savefig('log/'+fnamehead+key+'.png', format='png', dpi=200)
            fname = fnamehead + key + '.png'
            plt.savefig(str(self.save_dir/fname), format='png', dpi=200)
            plt.close()
